#include <vector>
#include <memory>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

#include "Mesh.hh"
#include "Shader.hh"
#include "Window.hh"
#include "Camera.hh"

using namespace std;

const float toRadians = 3.14159265f / 180.0f;

vector<unique_ptr<Mesh>> meshList;
vector<unique_ptr<Shader>> shaderList;

GLfloat deltaTime = 0.0f;
GLfloat lastTime = 0.0f;

const char *vShader = "../data/shader.vert";
const char *fShader = "../data/shader.frag";

void createObjects() {
	unsigned int indices[] = {
			0, 3, 1,
			1, 3, 2,
			2, 3, 0,
			0, 1, 2
	};

	GLfloat vertices[] = {
			-1.0f, -1.0f, -1.0f,
			0.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,
			0.0f, 1.0f, 0.0f
	};

	meshList.emplace_back(make_unique<Mesh>(vertices, indices, 12, 12));
	meshList.emplace_back(make_unique<Mesh>(vertices, indices, 12, 12));
	meshList.emplace_back(make_unique<Mesh>(vertices, indices, 12, 12));
	meshList.emplace_back(make_unique<Mesh>(vertices, indices, 12, 12));

	unsigned int indicesForBox[] = {
			0, 1, 3,
			3, 0, 2,
			2, 3, 7,
			7, 2, 6,
			6, 4, 5,
			5, 6, 7,
			7, 3, 1,
			1, 7, 5,
			5, 1, 0,
			0, 5, 4,
			4, 0, 2,
			2, 4, 6
	};

	GLfloat verticesForBox[] = {
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, -1.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,
			-1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f,
			-1.0f, -1.0f, -1.0f
	};

	meshList.emplace_back(make_unique<Mesh>(verticesForBox, indicesForBox, 24, 36));
}

void createShaders() {
	shaderList.emplace_back(make_unique<Shader>(vShader, fShader));
}

int main() {
	Window win(1400, 1000);
	win.initialize();

	createObjects();
	createShaders();

	Camera cam;

	glm::mat4 projection = glm::perspective(
			45.0f, // FOV
			win.getAspectRatio(),
			0.1f,
			100.0f
	);

	GLuint uniformModel, uniformProjection, uniformView;
	float angle = 0;

	// .:: main loop
	while (!win.shouldClose()) {

		GLfloat now = glfwGetTime();
		deltaTime = now - lastTime;
		lastTime = now;

		glfwPollEvents();
		cam.keyControl(win.getKeys(), deltaTime);
		cam.mouseControl(win.getXChange(), win.getYChange());

		glClearColor(0.8f, 0.0f, 0.4f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		shaderList.at(0)->use();
		uniformModel = shaderList.at(0)->getModelLocation();
		uniformProjection = shaderList.at(0)->getProjectionLocation();
		uniformView = shaderList.at(0)->getViewLocation();

		glm::mat4 model(1.0f);
		model = glm::translate(model, glm::vec3(0.0f, 0.7f, -2.5f));
		model = glm::rotate(model, angle * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.4f, 0.4, 0.4f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		glUniformMatrix4fv(uniformProjection, 1, GL_FALSE, glm::value_ptr(projection));
		glUniformMatrix4fv(uniformView, 1, GL_FALSE, glm::value_ptr(cam.calculateViewMatrix()));
		meshList.at(0)->render();
		angle++;

		model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(0.0f, -0.5f, -2.5f));
		model = glm::rotate(model, (angle / 2) * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::scale(model, glm::vec3(0.4f, 0.4, 0.4f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		meshList.at(1)->render();

		model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(-3.0f, -0.5f, -4.5f));
		model = glm::scale(model, glm::vec3(0.4f, 0.4, 0.4f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		meshList.at(2)->render();

		model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(3.0f, 0.0f, -8.5f));
		model = glm::scale(model, glm::vec3(0.4f, 0.4, 0.4f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		meshList.at(3)->render();

		model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(-40.0f, 0.0f, -8.5f));
		model = glm::scale(model, glm::vec3(5.0f, 6.0, 10.0f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		meshList.at(4)->render();

		model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(-40.0f, 12.0f, -8.5f));
		model = glm::scale(model, glm::vec3(5.0f, 3.0, 10.0f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		meshList.at(4)->render();

		model = glm::mat4(1.0f);
		model = glm::translate(model, glm::vec3(-40.0f, 20.0f, -8.5f));
		model = glm::rotate(model, (angle / 2) * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::scale(model, glm::vec3(5.0f, 3.0, 10.0f));
		glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
		meshList.at(4)->render();

		for (int i = 1; i < 10;i++) {
            model = glm::mat4(1.0f);
            model = glm::translate(model, glm::vec3(1.0f, i * 5.0f, -60.5f));
            model = glm::rotate(model, (angle + i * 6) * toRadians, glm::vec3(0.0f, 1.0f, 0.0f));
            model = glm::scale(model, glm::vec3(15.0f - i, 2.0, 10.0f));
            glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
            meshList.at(4)->render();
		}

		glUseProgram(0);

		win.swapBuffers();
	}

	return 0;
}
