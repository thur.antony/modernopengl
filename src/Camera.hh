#pragma once

#include <vector>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

class Camera {
public:
	explicit Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f),
					glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f),
					GLfloat yaw = -90.0f,
					GLfloat pitch = 0.0f,
					GLfloat moveSpeed = 5.0f,
					GLfloat turnSpeed = 0.5f);

	~Camera();

	void keyControl(const std::vector<bool> &keys, GLfloat deltaTime);
	void mouseControl(GLfloat xChange, GLfloat yChange);

	glm::mat4 calculateViewMatrix();

private:
	glm::vec3 position;
	glm::vec3 front;
	glm::vec3 up;
	glm::vec3 right;

	glm::vec3 worldUp;

	GLfloat yaw; // move around vertical axis
	GLfloat pitch; // move around horizontal axis
	// GLfloat roll;

	GLfloat moveSpeed;
	GLfloat turnSpeed;

	void update();
};
