#pragma once

#include <fstream>
#include <iostream>
#include <string>
#include <cstring>

#include <GL/glew.h>


class Shader {
public:
	Shader();
	Shader(const char* vertexCode, const char* fragmentCode);
	~Shader();

	void createFromString(const char *vertexCode, const char* fragmentCode);
	void createFormFile(const char *vertFile, const char* fragFile);
	GLuint getProjectionLocation();
	GLuint getModelLocation();
	GLuint getViewLocation();

	void use();
	void clear();

private:
	GLuint shaderID, uniformProjection, uniformModel, uniformView;

	void compileShader(const char *vertexCode, const char *fragmentCode);
	void addShader(const char* shaderCode, GLenum shaderType);
	std::string readFile(const char *fileLocation);
};
