#include "Camera.hh"

Camera::Camera(glm::vec3 position, glm::vec3 up, GLfloat yaw, GLfloat pitch, GLfloat moveSpeed, GLfloat turnSpeed)
		: position(position), front(glm::vec3(0.0f, 0.0f, -1.0f)), worldUp(up),
		  yaw(yaw), pitch(pitch),
		  moveSpeed(moveSpeed), turnSpeed(turnSpeed) {

	update();
}

Camera::~Camera() = default;

void Camera::keyControl(const std::vector<bool> &keys, GLfloat deltaTime) {

	GLfloat velocity = moveSpeed * deltaTime;

	if (keys.at(GLFW_KEY_W)) {
		position += front * velocity;
	}
	if (keys.at(GLFW_KEY_S)) {
		position -= front * velocity;
	}
	if (keys.at(GLFW_KEY_A)) {
		position -= right * velocity;
	}
	if (keys.at(GLFW_KEY_D)) {
		position += right * velocity;
	}
}

void Camera::mouseControl(GLfloat xChange, GLfloat yChange) {
	xChange *= turnSpeed;
	yChange *= turnSpeed;

	yaw += xChange;
	pitch += yChange;

	if (pitch > 89.0f) pitch = 89.0f;
	if (pitch < -89.0f) pitch = -89.0f;

	update();
}

glm::mat4 Camera::calculateViewMatrix() {
	return glm::lookAt(position, position + front, up);
}

void Camera::update() {
	front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	front.y = sin(glm::radians(pitch));
	front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	front = glm::normalize(front);

	right = glm::normalize(glm::cross(front, worldUp));
	up = glm::normalize(glm::cross(right, front));
}
