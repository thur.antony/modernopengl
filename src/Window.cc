#include "Window.hh"

Window::Window(GLint windowWidth, GLint windowHeight) :
		mainWindow(nullptr),
		width(windowWidth), height(windowHeight), keys(1024), mouseFirstMoved(true),
		lastX(0), lastY(0), xChange(0), yChange(0) {
	std::cout << "Window created" << std::endl;
}

Window::~Window() {
	glfwDestroyWindow(mainWindow);
	glfwTerminate();
	std::cout << "Window deleted" << std::endl;
}

int Window::initialize() {

	// .:: initialisation of GLFW
	if (!glfwInit()) {
		std::cout << "[Error] can't init glfw" << std::endl;
		glfwTerminate();
		return 1;
	}

	// .:: setup GLFW window properties
	// OpenGL version
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	// .:: create GLFW window
	mainWindow = glfwCreateWindow(width, height, "Test window", nullptr, nullptr);
	if (!mainWindow) {
		std::cout << "[Error] can't create GLFW window" << std::endl;
		glfwTerminate();
		return 1;
	}

	// .:: Get buffer size information - buffer will hold all of the openGL data
	glfwGetFramebufferSize(mainWindow, &bufferWidth, &bufferHeight);
	std::cout << "[bufferSize] width: " << bufferWidth << " height: " << bufferHeight << std::endl;

	// .:: Set context for GLFW to use
	glfwMakeContextCurrent(mainWindow);

	// .:: Handle Keyboard and Mouse input
	createCallbacks();
	// .:: Lock Cursor
	glfwSetInputMode(mainWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);


	// .:: Allow modern extension features
	glewExperimental = GL_TRUE;

	// .:: Initialize GLEW
	if (glewInit() != GLEW_OK) {
		std::cout << "[Error] can't initialize GLEW " << std::endl;
		glfwDestroyWindow(mainWindow);
		glfwTerminate();
		return 1;
	}

	glEnable(GL_DEPTH_TEST);

	// .:: Setup Viewport size
	glViewport(0, 0, bufferWidth, bufferHeight);

	glfwSetWindowUserPointer(mainWindow, this);
}

void Window::createCallbacks() {
	glfwSetKeyCallback(mainWindow, handleKeys);
	glfwSetCursorPosCallback(mainWindow, handleMouse);
}

void Window::handleKeys(GLFWwindow *window, int key, int code, int action, int mode) {
	auto *win = static_cast<Window *>(glfwGetWindowUserPointer(window));

	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, GL_TRUE);
	}

	if (key >= 0 && key < 1024) {
		if (action == GLFW_PRESS) {
			win->keys.at(key) = true;
		} else if (action == GLFW_RELEASE) {
			win->keys[key] = false;
		}
	}
}

void Window::handleMouse(GLFWwindow *window, double xPos, double yPos) {
	auto *win = static_cast<Window *>(glfwGetWindowUserPointer(window));

	if (win->mouseFirstMoved) {
		win->lastX = xPos;
		win->lastY = yPos;
		win->mouseFirstMoved = false;
	}

	win->xChange = xPos - win->lastX;
	win->yChange = yPos - win->lastY;

	win->lastX = xPos;
	win->lastY = yPos;
}

GLfloat Window::getXChange() {
	GLfloat change = xChange;
	xChange = 0.0;
	return change;
}

GLfloat Window::getYChange() {
	GLfloat change = yChange;
	yChange = 0.0;
	return change;
}

const std::vector<bool> &Window::getKeys() const {
	return keys;
}


