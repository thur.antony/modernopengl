#pragma once

#include <GL/glew.h>
#include <iostream>

class Mesh {
public:
    Mesh();
    Mesh(GLfloat *vertices, unsigned int *indices, unsigned int numOfVertices, unsigned int numOfIndices);
    ~Mesh();

    void create(GLfloat *vertices, unsigned int *indices, unsigned int numOfVertices, unsigned int numOfIndices);
    void render();
    void clear();
private:
    GLuint VAO, VBO, IBO;
    GLsizei indexCount;
};

