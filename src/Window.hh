#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <vector>

class Window {
public:
	explicit Window(GLint windowWidth = 800, GLint windowHeight = 600);
	~Window();

	int initialize();
	GLint getBufferWidth() { return bufferWidth; }
	GLint getBufferHeight() { return bufferHeight; }
	GLfloat getAspectRatio() { return static_cast<GLfloat>(bufferWidth) / static_cast<GLfloat>(bufferHeight); }
	bool shouldClose() { return glfwWindowShouldClose(mainWindow); }
	void swapBuffers() { glfwSwapBuffers(mainWindow); }

	const std::vector<bool> &getKeys() const;
	GLfloat getXChange();
	GLfloat getYChange();

private:
	GLFWwindow *mainWindow;
	GLint width, height;
	GLint bufferWidth, bufferHeight;

	std::vector<bool> keys;

	GLfloat lastX, lastY;
	GLfloat xChange, yChange;
	bool mouseFirstMoved;

	void createCallbacks();
	static void handleKeys(GLFWwindow *window, int key, int code, int action, int mode);
	static void handleMouse(GLFWwindow *window, double xPos, double yPos);
};
