#include "Shader.hh"

Shader::Shader() : shaderID(0), uniformProjection(0), uniformModel(0), uniformView(0) {
	std::cout << "Shader created" << std::endl;
}

Shader::Shader(const char *vertexFile, const char *fragFile) : Shader() {
	createFormFile(vertexFile, fragFile);
}

Shader::~Shader() {
	clear();
	std::cout << "Shader deleted" << std::endl;
}

void Shader::createFromString(const char *vertexCode, const char *fragmentCode) {
	compileShader(vertexCode, fragmentCode);
}

void Shader::createFormFile(const char *vertFile, const char *fragFile) {
	createFromString(readFile(vertFile).c_str(), readFile(fragFile).c_str());
}

std::string Shader::readFile(const char *fileLocation) {
	std::string content;
	std::ifstream filestream(fileLocation, std::ios::in);

	if (!filestream.is_open()) {
		std::cout << "failed to read file from: " << fileLocation << std::endl;
		return "";
	}

	std::string line;
	while (!filestream.eof()) {
		std::getline(filestream, line);
		content.append(line + "\n");
	}

	filestream.close();
	return content;
}

GLuint Shader::getProjectionLocation() {
	return uniformProjection;
}

GLuint Shader::getModelLocation() {
	return uniformModel;
}

GLuint Shader::getViewLocation() {
	return uniformView;
}

void Shader::use() {
	glUseProgram(shaderID);
}

void Shader::clear() {
	if (shaderID) {
		glDeleteProgram(shaderID);
		shaderID = 0;
	}
	uniformModel = 0;
	uniformProjection = 0;
}

void Shader::compileShader(const char *vertexCode, const char *fragmentCode) {
	shaderID = glCreateProgram();

	if (!shaderID) {
		std::cout << "[Error] can't create shader" << std::endl;
	}

	addShader(vertexCode, GL_VERTEX_SHADER);
	addShader(fragmentCode, GL_FRAGMENT_SHADER);

	GLint result = 0;
	GLchar eLog[1024] = {0};

	glLinkProgram(shaderID);
	glGetProgramiv(shaderID, GL_LINK_STATUS, &result);
	if (!result) {
		glGetProgramInfoLog(shaderID, sizeof(eLog), nullptr, eLog);
		std::cout << "link status: " << eLog << std::endl;
	}

	glValidateProgram(shaderID);
	glGetProgramiv(shaderID, GL_VALIDATE_STATUS, &result);
	if (!result) {
		glGetProgramInfoLog(shaderID, sizeof(eLog), nullptr, eLog);
		std::cout << "validate status " << eLog << std::endl;
	}

	uniformModel = glGetUniformLocation(shaderID, "model");
	uniformProjection = glGetUniformLocation(shaderID, "projection");
	uniformView = glGetUniformLocation(shaderID, "view");
}

void Shader::addShader(const char *shaderCode, GLenum shaderType) {
	GLuint theShader = glCreateShader(shaderType); // emtpy shader

	const GLchar *theCode[1];
	theCode[0] = shaderCode;

	GLint codeLength[1];
	codeLength[0] = strlen(shaderCode);

	glShaderSource(theShader, 1, theCode, codeLength);
	glCompileShader(theShader);

	GLint result = 0;
	GLchar eLog[1024] = {0};

	glGetShaderiv(theShader, GL_COMPILE_STATUS, &result);
	if (!result) {
		glGetShaderInfoLog(theShader, sizeof(eLog), nullptr, eLog);
		std::cout << "shader compile status " << eLog << std::endl;
	}

	glAttachShader(shaderID, theShader);
}
