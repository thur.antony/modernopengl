cmake_minimum_required(VERSION 3.12)
project(modernopengl)

set(CMAKE_CXX_STANDARD 17)

find_package(OpenGL REQUIRED)
find_package(PkgConfig REQUIRED)

# opengl
find_package(OpenGL REQUIRED)
include_directories(${OPENGL_INCLUDE_DIRS})

# .:: glfw
pkg_search_module(GLFW REQUIRED glfw3)
include_directories(${GLFW_INCLUDE_DIRS})
link_libraries(${GLFW_LIBRARIES_DIRS})

# .:: glew
find_package(GLEW REQUIRED)
include_directories($(GLEW_INCLUDE_DIRS))
link_libraries(${GLEW_LIBRARIES})

# .:: glm
include_directories(lib)

set(SRC src/main.cc
        src/Mesh.cc
        src/Shader.cc
        src/Window.cc
        src/Camera.cc
        )

add_executable(${PROJECT_NAME} ${SRC})
target_link_libraries(${PROJECT_NAME}
        dl pthread GL Xi X11
        glfw3
        ${GLEW_LIBRARIES}
        )