#version 450

in vec4 vCol;
out vec4 color;

void    main(void) {
	color = vCol;
}
